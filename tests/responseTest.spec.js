const assert = require('chai').assert;
const Response = require('../src/Response');

describe('Response', function() {
    describe('#getResponse', function() {
        it('returns an object with body,errors,meta', function() {
            const firstResponse = new Response();
            const expected = {
                "data": {},
                "errors": [],
                "meta": {}
            };

            assert.deepEqual(firstResponse.getResponse(), expected);
            assert.hasAllKeys(firstResponse.getResponse(), ["data", "errors", "meta"]);
        });
    });
});