const assert = require('chai').assert;
const ContactsRepo = require('../../src/contacts/ContactsRepo');

describe('ContactsRepo', function() {
    describe('#get', function() {
        it('returns an array', function() {
            const allContacts = (new ContactsRepo()).get();

            assert.isArray(allContacts);
        });

        it('get all contacts', function() {
            const allContacts = (new ContactsRepo()).get();

            assert.includeMembers(allContacts, [1,2,3,4,5]);
        });
    });
});