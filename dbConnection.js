"use strict"

const mongoose = require('mongoose');

mongoose.connect(
    'mongodb+srv://' + process.env.DB_USER + ':' + process.env.DB_PASSWORD + '@cmcluster-rpzny.mongodb.net/' + process.env.DB_NAME,
    {
        useNewUrlParser: true
    }
)
.catch(error => {
    console.log(error);
});