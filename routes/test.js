const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const testController = require('../src/test/testController');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', testController.get);

router.post('/', testController.post);

// const User = require('./User');
module.exports = router;