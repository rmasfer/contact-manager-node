const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const contactController = require('../src/contacts/contactsController');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', contactController.get);
router.post('/', contactController.post);
router.delete('/', contactController.delete);

module.exports = router;