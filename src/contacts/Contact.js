"use strict"
const mongoose = require('mongoose');

const contactsSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId
    },
    username: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    githubUsername: {
        type: String,
        required: true
    }
});

mongoose.model('Contact', contactsSchema);

module.exports = mongoose.model('Contact');