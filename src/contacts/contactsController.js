const ContactRepo = require('./ContactsRepo');
const mongoose = require('mongoose');
const Contact = require('./Contact');
const Response = require('../Response');

exports.get = (req, res) => {
    Contact.find({}, (error, results) => {
        console.log(results);
        const allResponse = new Response(results, [], {});
        res.status(200).send(allResponse.getResponse());
    });
}

exports.post = (req, res) => {

    const newContact = {
        _id: mongoose.Types.ObjectId(),
        username : req.body.username,
        phone : req.body.phone,
        address : req.body.address,
        githubUsername : req.body.githubUsername,
    };

    Contact.create(newContact, (error, results) => {
        console.log(error);
        console.log(results);
        const allResponse = new Response(newContact, [], {});
        res.status(200).send(allResponse.getResponse());
    });
}

exports.delete = (req, res) => {
    const contactId = req.body.id;
    Contact.deleteOne({_id: contactId}, (error, results) => {
        const allResponse = new Response({"deletedId": contactId}, [], {});
        res.status(200).send(allResponse.getResponse());
    });
}