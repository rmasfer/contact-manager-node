exports.get = (req, res) => {
    const body = {
        "data": [
            {"id" : 1},
            {"id" : 2}
        ],
        "errors": [],
        "meta": {}
    };
    res.status(200).send(body);
}

exports.post = (req, res) => {

    const body = {
        "data": {
            "id" : 1,
        },
        "errors": [],
        "meta": {}
    };

    res.status(200).send(body);
}