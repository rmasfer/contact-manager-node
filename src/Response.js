"use strict"

class Response {
    constructor(data = {}, errors = [], meta = {}) {
        this.data = data;
        this.errors = errors;
        this.meta = meta;
    }

    getResponse() {
        return {
            "data": this.data,
            "errors": this.errors,
            "meta": this.meta
        };
    }
}

module.exports = Response;